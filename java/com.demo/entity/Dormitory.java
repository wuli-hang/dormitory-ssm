package com.southwind.entity;

import lombok.Data;

// 使用 Lombok 自动生成 getter/setter 方法、toString 方法等常用的方法
@Data
public class Dormitory {

    // 宿舍的唯一标识
    private Integer id;

    // 宿舍所在的楼栋的唯一标识
    private Integer buildingId;

    // 宿舍所在的楼栋的名称
    private String buildingName;

    // 宿舍的名称
    private String name;

    // 宿舍的类型
    private Integer type;

    // 宿舍的空闲情况
    private Integer available;

    // 宿舍管理员的联系电话
    private String telephone;

}