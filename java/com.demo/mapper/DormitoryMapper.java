package com.southwind.mapper;

import com.southwind.entity.Dormitory;

import java.util.List;

public interface DormitoryMapper {
    //查询所有可用的宿舍列表
    public List<Dormitory> availableList();
    //将指定 id 的宿舍的可用数量减 1
    public void subAvailable(Integer id);
    //将指定 id 的宿舍的可用数量加 1
    public void addAvailable(Integer id);
    //根据指定的楼栋 id 查询该楼栋下的所有宿舍 id
    public List<Integer> findDormitoryIdByBuildingId(Integer buildingId);
    //查询一个可用的宿舍 id
    public Integer findAvailableDormitoryId();
    //：根据指定的宿舍 id 删除该宿舍
    public void delete(Integer id);
    //查询所有宿舍列表
    public List<Dormitory> list();
    //根据指定的宿舍名称模糊查询宿舍列表
    public List<Dormitory> searchByName(String value);
    //根据指定的宿舍电话模糊查询宿舍列表
    public List<Dormitory> searchByTelephone(String value);
    //保存一个宿舍
    public void save(Dormitory dormitory);
    //更新一个宿舍
    public void update(Dormitory dormitory);
    //根据指定的楼栋 id 查询该楼栋下的所有宿舍。
    public List<Dormitory> findByBuildingId(Integer buildingId);
}
