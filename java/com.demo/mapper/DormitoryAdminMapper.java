package com.southwind.mapper;

import com.southwind.entity.DormitoryAdmin;

import java.util.List;

public interface DormitoryAdminMapper {
    //* list()：返回所有的 DormitoryAdmin 实体列表。
    public List<DormitoryAdmin> list();
    //searchByUsername(String value)：根据用户名搜索 DormitoryAdmin 实体列表。
    public List<DormitoryAdmin> searchByUsername(String value);
    //searchByName(String value)：根据姓名搜索 DormitoryAdmin 实体列表。
    public List<DormitoryAdmin> searchByName(String value);
    //searchByTelephone(String value)：根据电话号码搜索 DormitoryAdmin 实体列表。
    public List<DormitoryAdmin> searchByTelephone(String value);
    //save(DormitoryAdmin dormitoryAdmin)：保存一个 DormitoryAdmin 实体到数据库中。
    public void save(DormitoryAdmin dormitoryAdmin);
    //delete(Integer id)：根据 id 删除一个 DormitoryAdmin 实体。
    public void delete(Integer id);
    //update(DormitoryAdmin dormitoryAdmin)：更新一个 DormitoryAdmin 实体。
    public void update(DormitoryAdmin dormitoryAdmin);
    //findByUserName(String username)：根据用户名查找一个 DormitoryAdmin 实体。
    public DormitoryAdmin findByUserName(String username);
}
