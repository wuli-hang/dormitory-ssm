package com.southwind.service.impl;

import com.southwind.entity.Dormitory;
import com.southwind.mapper.DormitoryMapper;
import com.southwind.mapper.StudentMapper;
import com.southwind.service.DormitoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DormitoryServiceImpl implements DormitoryService {

    @Autowired //自动装载DormitoryMapper和StudentMapper两个数据访问类
    private DormitoryMapper dormitoryMapper;
    @Autowired
    private StudentMapper studentMapper;

    @Override
    public List<Dormitory> availableList() { //定义availableList方法，返回所有空闲宿舍列表
        return this.dormitoryMapper.availableList(); //调用dormitoryMapper的availableList方法，返回所有空闲宿舍列表
    }

    @Override
    public List<Dormitory> list() { //定义list方法，返回所有宿舍列表
        return this.dormitoryMapper.list(); //调用dormitoryMapper的list方法，返回所有宿舍列表
    }

    @Override
    public List<Dormitory> search(String key, String value) { //定义search方法，根据关键字和对应的值进行宿舍列表的搜索操作
        if(value.equals("")) return this.dormitoryMapper.list(); //如果value为空字符串，返回所有宿舍列表
        List<Dormitory> list = null; //创建一个新的Dormitory类型的列表
        switch (key){ //根据key的值进行搜索
            case "name": //当key为name时，根据名称进行搜索
                list = this.dormitoryMapper.searchByName(value);
                break;
            case "telephone": //当key为telephone时，根据电话号码进行搜索
                list = this.dormitoryMapper.searchByTelephone(value);
                break;
        }
        return list; //返回查询得到的宿舍列表
    }

    @Override
    public void save(Dormitory dormitory) { //定义save方法，将指定的宿舍信息保存到数据库中
        try {
            this.dormitoryMapper.save(dormitory); //调用dormitoryMapper的save方法，将指定的宿舍信息保存到数据库中
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void update(Dormitory dormitory) { //定义update方法，将指定的宿舍信息更新到数据库中
        try {
            this.dormitoryMapper.update(dormitory); //调用dormitoryMapper的update方法，将指定的宿舍信息更新到数据库中
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void delete(Integer id) { //定义delete方法，删除指定ID的宿舍信息
        try {
            List<Integer> studentIdList = this.studentMapper.findStudentIdByDormitoryId(id); //获取该宿舍下的所有学生ID列表
            for (Integer studentId : studentIdList) { //遍历学生ID列表
                Integer availableDormitoryId = this.dormitoryMapper.findAvailableDormitoryId(); //在所有空闲宿舍中查找一间可用的宿舍
                this.studentMapper.resetDormitoryId(studentId, availableDormitoryId); //将该学生重新分配到可用的宿舍中
                this.dormitoryMapper.subAvailable(availableDormitoryId); //将该可用宿舍的可用床位数量减1
            }
            this.dormitoryMapper.delete(id); //删除该宿舍的信息
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Dormitory> findByBuildingId(Integer buildingId) { //定义findByBuildingId方法，返回指定建筑物ID下的所有宿舍列表
        return this.dormitoryMapper.findByBuildingId(buildingId); //调用dormitoryMapper的findByBuildingId方法，返回指定建筑物ID下的所有宿舍列表
    }
}
